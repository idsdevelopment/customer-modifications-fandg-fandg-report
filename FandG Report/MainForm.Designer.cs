﻿namespace FandGReport
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.CloseSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.MainTabControl = new System.Windows.Forms.TabControl();
			this.MainTabPage = new System.Windows.Forms.TabPage();
			this.StatusLabel = new System.Windows.Forms.Label();
			this.GetCsvBtn = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.Date = new System.Windows.Forms.DateTimePicker();
			this.ImportSettingsTabPage = new System.Windows.Forms.TabPage();
			this.button5 = new System.Windows.Forms.Button();
			this.label13 = new System.Windows.Forms.Label();
			this.CsvExportPath = new System.Windows.Forms.TextBox();
			this.DebugMode = new System.Windows.Forms.CheckBox();
			this.OpenImportFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.menuStrip1.SuspendLayout();
			this.MainTabControl.SuspendLayout();
			this.MainTabPage.SuspendLayout();
			this.ImportSettingsTabPage.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.CloseSettings});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(588, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
			this.exitToolStripMenuItem.Text = "&Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferencesToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// preferencesToolStripMenuItem
			// 
			this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem});
			this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
			this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
			this.preferencesToolStripMenuItem.Text = "Preferences";
			// 
			// importToolStripMenuItem
			// 
			this.importToolStripMenuItem.Name = "importToolStripMenuItem";
			this.importToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.importToolStripMenuItem.Text = "IDS Settings";
			this.importToolStripMenuItem.Click += new System.EventHandler(this.ImportSettingsMenuItem_Click);
			// 
			// CloseSettings
			// 
			this.CloseSettings.Enabled = false;
			this.CloseSettings.Name = "CloseSettings";
			this.CloseSettings.Size = new System.Drawing.Size(93, 20);
			this.CloseSettings.Text = "Close Settings";
			this.CloseSettings.Click += new System.EventHandler(this.closeSettingsToolStripMenuItem_Click);
			// 
			// MainTabControl
			// 
			this.MainTabControl.Controls.Add(this.MainTabPage);
			this.MainTabControl.Controls.Add(this.ImportSettingsTabPage);
			this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainTabControl.Location = new System.Drawing.Point(0, 24);
			this.MainTabControl.Name = "MainTabControl";
			this.MainTabControl.SelectedIndex = 0;
			this.MainTabControl.Size = new System.Drawing.Size(588, 180);
			this.MainTabControl.TabIndex = 2;
			// 
			// MainTabPage
			// 
			this.MainTabPage.Controls.Add(this.StatusLabel);
			this.MainTabPage.Controls.Add(this.GetCsvBtn);
			this.MainTabPage.Controls.Add(this.label1);
			this.MainTabPage.Controls.Add(this.Date);
			this.MainTabPage.Location = new System.Drawing.Point(4, 22);
			this.MainTabPage.Name = "MainTabPage";
			this.MainTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.MainTabPage.Size = new System.Drawing.Size(580, 154);
			this.MainTabPage.TabIndex = 0;
			this.MainTabPage.Text = "Main";
			this.MainTabPage.UseVisualStyleBackColor = true;
			// 
			// StatusLabel
			// 
			this.StatusLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.StatusLabel.Location = new System.Drawing.Point(3, 3);
			this.StatusLabel.Name = "StatusLabel";
			this.StatusLabel.Size = new System.Drawing.Size(574, 22);
			this.StatusLabel.TabIndex = 6;
			this.StatusLabel.Text = "label3";
			this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// GetCsvBtn
			// 
			this.GetCsvBtn.Location = new System.Drawing.Point(251, 49);
			this.GetCsvBtn.Name = "GetCsvBtn";
			this.GetCsvBtn.Size = new System.Drawing.Size(84, 23);
			this.GetCsvBtn.TabIndex = 5;
			this.GetCsvBtn.Text = "Get Csv";
			this.GetCsvBtn.UseVisualStyleBackColor = true;
			this.GetCsvBtn.Click += new System.EventHandler(this.GetCsvBtn_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(33, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Date:";
			// 
			// Date
			// 
			this.Date.Location = new System.Drawing.Point(10, 49);
			this.Date.Name = "Date";
			this.Date.Size = new System.Drawing.Size(201, 20);
			this.Date.TabIndex = 1;
			// 
			// ImportSettingsTabPage
			// 
			this.ImportSettingsTabPage.Controls.Add(this.button5);
			this.ImportSettingsTabPage.Controls.Add(this.label13);
			this.ImportSettingsTabPage.Controls.Add(this.CsvExportPath);
			this.ImportSettingsTabPage.Controls.Add(this.DebugMode);
			this.ImportSettingsTabPage.Location = new System.Drawing.Point(4, 22);
			this.ImportSettingsTabPage.Name = "ImportSettingsTabPage";
			this.ImportSettingsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.ImportSettingsTabPage.Size = new System.Drawing.Size(580, 99);
			this.ImportSettingsTabPage.TabIndex = 1;
			this.ImportSettingsTabPage.Text = "IDS Settings";
			this.ImportSettingsTabPage.UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(485, 26);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 19;
			this.button5.Text = "Browse";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(5, 13);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(89, 13);
			this.label13.TabIndex = 17;
			this.label13.Text = "CSV Export Path:";
			// 
			// CsvExportPath
			// 
			this.CsvExportPath.Location = new System.Drawing.Point(8, 29);
			this.CsvExportPath.Name = "CsvExportPath";
			this.CsvExportPath.Size = new System.Drawing.Size(449, 20);
			this.CsvExportPath.TabIndex = 18;
			this.CsvExportPath.Text = "C:\\Ids\\Export";
			// 
			// DebugMode
			// 
			this.DebugMode.AutoSize = true;
			this.DebugMode.Location = new System.Drawing.Point(8, 72);
			this.DebugMode.Name = "DebugMode";
			this.DebugMode.Size = new System.Drawing.Size(88, 17);
			this.DebugMode.TabIndex = 16;
			this.DebugMode.Text = "Debug Mode";
			this.DebugMode.UseVisualStyleBackColor = true;
			// 
			// OpenImportFileDialog
			// 
			this.OpenImportFileDialog.Filter = "Csv File|*.csv";
			// 
			// FolderBrowserDialog
			// 
			this.FolderBrowserDialog.Description = "Select Path";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(588, 204);
			this.Controls.Add(this.MainTabControl);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "F&G Report";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.MainTabControl.ResumeLayout(false);
			this.MainTabPage.ResumeLayout(false);
			this.MainTabPage.PerformLayout();
			this.ImportSettingsTabPage.ResumeLayout(false);
			this.ImportSettingsTabPage.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.TabControl MainTabControl;
		private System.Windows.Forms.TabPage MainTabPage;
		private System.Windows.Forms.TabPage ImportSettingsTabPage;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog OpenImportFileDialog;
		private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem CloseSettings;
		private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
		private System.Windows.Forms.CheckBox DebugMode;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TextBox CsvExportPath;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker Date;
		private System.Windows.Forms.Button GetCsvBtn;
		private System.Windows.Forms.Label StatusLabel;
	}
}

