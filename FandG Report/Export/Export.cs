﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FandGReport.Export
{
	internal class IdsInvoiceExport
	{
		private class ErrorEntry
		{
			internal string Error;
			internal int LineNumber;
		}

		private class ErrorList : List<ErrorEntry>
		{
			internal void Add( string _Error, int _LineNumber )
			{
				base.Add( new ErrorEntry { Error = _Error, LineNumber = _LineNumber } );
			}

			public override string ToString()
			{
				var Retval = new StringBuilder();

				foreach( var Entry in this )
					Retval.Append( "Line Number: " + Entry.LineNumber.ToString() + ",  Error: " + Entry.Error + "\r\n" );

				return Retval.ToString();
			}

			internal void WriteToStream( Stream stream )
			{
				using( var Writer = new StreamWriter( stream, System.Text.Encoding.ASCII ) )
				{
					Writer.Write( ToString() );
					Writer.Flush();
					Writer.Close();
				}
			}
		}

		internal class HEADER_COL
		{
			internal const string ACCOUNT = "1",
									TRIP = "2",
									CHARGE_LINE = "3",		// Charges must be greater than trips for sort
									END_OF_INVOICE = "4",
									TEMPORY_CHARGE_HOLDER = "5";		// Appended to end and them set back to CHARGE_LINE

		}

		private class ACCOUNT_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,			// B
				INVOICE_NUMBER,			// C
				TRIP_ID,				// D

				INVOICE_DATE_CREATED,	// E
				READY_TIME,				// F
				INVOICE_TO_DATE,		// G
				CALL_TIME,				// H

				PICKUP_TIME,			// I
				DELIVERY_TIME,			// J

				PICKUP_REFERENCE,		// K
				PICKUP_COMPANY,			// L
				PICKUP_ADDRESS_1,		// M
				PICKUP_ADDRESS_2,		// N
				PICKUP_CITY,			// O
				PICKUP_REGION,			// P
				PICKUP_COUNTRY,			// Q
				PICKUP_POST_CODE,		// R
				PICKUP_ZONE,			// S
				PICKUP_CONTACT,			// T
				PICKUP_TELEPHONE,		// U
				PICKUP_EMAIL,			// V
				PICKUP_NOTES,			// W

				DELIVERY_REFERENCE,		// X
				DELIVERY_COMPANY,		// Y
				DELIVERY_ADDRESS_1,		// Z
				DELIVERY_ADDRESS_2,		// AA
				DELIVERY_CITY,			// AB
				DELIVERY_REGION,		// AC
				DELIVERY_POST_CODE,		// AD
				DELIVERY_ZONE,			// AE
				DELIVERY_COUNTRY,		// AF
				DELIVERY_CONTACT,		// AG
				DELIVERY_TELEPHONE,		// AH
				DELIVERY_EMAIL,			// AI
				DELIVERY_NOTES,			// AJ

				BILLING_COMPANY,		// AK
				BILLING_ADDRESS_1,		// AL
				BILLING_ADDRESS_2,		// AM
				BILLING_CITY,			// AN
				BILLING_REGION,			// AO
				BILLING_POST_CODE,		// AP
				BILLING_COUNTRY,		// AQ
				BILLING_CONTACT,		// AR
				BILLING_TELEPHONE,		// AS
				BILLING_EMAIL,			// AT

				SHIPPING_COMPANY,		// AU	
				SHIPPING_ADDRESS_1,		// AV
				SHIPPING_ADDRESS_2,		// AW
				SHIPPING_CITY,			// AX
				SHIPPING_REGION,		// AY
				SHIPPING_POST_CODE,		// AZ
				SHIPPING_COUNTRY,		// BA
				SHIPPING_CONTACT,		// BB
				SHIPPING_TELEPHONE,		// BC
				SHIPPING_EMAIL,			// BD

				DRIVER_NAME,			// BE
				CALL_TAKER_ID,			// BF

				SORT_ORDER,				// BG
				DRIVER_REFERENCE,		// BH		
			}
		}

		internal class TRIP_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,			// B
				INVOICE_NUMBER,			// C
				TRIP_ID,				// D

				SERVICE_LEVEL,			// E
				PACKAGE_TYPE,			// F
				PIECES,					// G
				WEIGHT,					// H
				BASE_RATE,				// I
				TOTAL_LIVE_VALUE,		// J			+ Discount
				DISCOUNT_AMOUNT,		// K
			}
		}

		internal class CHARGE_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER,
				TRIP_ID,

				DESCRIPTION,
				VALUE
			}
		}

		internal class END_OF_INVOICE_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER,
				TRIP_ID,					// Gets fudged	~~~~~~~~~~~~~~~~~
				INVOICE_TOTAL
			}
		}

		ErrorList Errors;
		string ErrorPath, ExportPath;

		internal bool HasErrors
		{
			get
			{
				return ( Errors.Count > 0 );
			}
		}


		public IdsInvoiceExport( string exportPath, string errorPath )
		{
			ErrorPath = Utils.AddPathSeparator( errorPath );
			Directory.CreateDirectory( ErrorPath );

			ExportPath = Utils.AddPathSeparator( exportPath );
			Directory.CreateDirectory( ExportPath );
		}

		private static int MaxEnum( Type enumType )
		{
			return Enum.GetValues( enumType ).GetUpperBound( 0 );
		}


		private static string FileNumberToString( int fieldNumber )
		{
			var Num = fieldNumber.ToString();
			fieldNumber--;

			var CsvCol = "";

			if( fieldNumber > 26 )
			{
				CsvCol = "A";
				fieldNumber %= 26;
			}

			CsvCol += ( (char)( (int)'A' + (int)fieldNumber ) ).ToString();

			return ( ":" + Num + "  (" + CsvCol + ")." );
		}

		public bool ExportCsv( Stream csvStream, bool debugMode, string csvExportPath, string errorPath, out int highestInvoiceNumber, out string errorText )
		{
			errorPath = Utils.AddPathSeparator( errorPath );
			csvExportPath = Utils.AddPathSeparator( csvExportPath );

			highestInvoiceNumber = 0;
			errorText = "";

			var Reader = new Csv( csvStream );

			Action<string> DoDebug = ( fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						Csv.WriteToFile( Reader, errorPath + fileName + Globals.CSV_EXTENSION );
					}
					catch
					{
					}
				}
			};

			var CurrentRecordIndex = 0;

			Errors = new ErrorList();

			Row Cells = null;

			try
			{
				Action<int> IsValidDecimal = ( int FieldNumber ) =>
				{
					decimal Temp;
					if( !decimal.TryParse( Cells[ FieldNumber ].AsString.Trim(), out Temp ) )
						Errors.Add( "Invalid numeric field" + FileNumberToString( FieldNumber ), CurrentRecordIndex + 1 );
				};

				Action<int> IsNotBlank = ( int FieldNumber ) =>
				{
					if( string.IsNullOrEmpty( Cells[ FieldNumber ].AsString.Trim() ) )
						Errors.Add( "Field cannot be blank" +  FileNumberToString( FieldNumber ), CurrentRecordIndex + 1 );
				};


				Action<int> IsValidFieldCount = ( int FieldCount ) =>
				{
					if( Cells.Count < FieldCount )
						Errors.Add( "Invalid field count", CurrentRecordIndex + 1 );
				};

				Action<int> IsValidTaxId = ( int FieldNumber ) =>
				{
					switch( Cells[ FieldNumber ].AsString.Trim().ToUpper() )
					{
					case "HST":
					case "GST":
					case "PST":
						break;
					default:
						Errors.Add( "Invalid TaxId", CurrentRecordIndex + 1 );
						break;
					}
				};

				// Pre Parse Checking For Errors
				var Rows = Reader.Rows;
				double InvoiveTotal = 0;

				for( CurrentRecordIndex = 0; CurrentRecordIndex < Rows; ++CurrentRecordIndex )
				{
					Cells = Reader[ CurrentRecordIndex ];
					if( Cells.Count > 0 )
					{
						switch( Cells[ 0 ].AsString.Trim() )
						{
						case HEADER_COL.ACCOUNT:
							IsValidFieldCount( MaxEnum( typeof( ACCOUNT_LINE.FIELDS ) ) );
							IsNotBlank( (int)ACCOUNT_LINE.FIELDS.ACCOUNT_ID );
							IsNotBlank( (int)ACCOUNT_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)ACCOUNT_LINE.FIELDS.TRIP_ID );
							IsNotBlank( (int)ACCOUNT_LINE.FIELDS.INVOICE_TO_DATE );
							break;

						case HEADER_COL.TRIP:
							IsValidFieldCount( MaxEnum( typeof( TRIP_LINE.FIELDS ) ) );
							IsNotBlank( (int)TRIP_LINE.FIELDS.ACCOUNT_ID );
							IsNotBlank( (int)TRIP_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)TRIP_LINE.FIELDS.TRIP_ID );
							IsNotBlank( (int)TRIP_LINE.FIELDS.SERVICE_LEVEL );
							IsNotBlank( (int)TRIP_LINE.FIELDS.PACKAGE_TYPE );
							IsValidDecimal( (int)TRIP_LINE.FIELDS.BASE_RATE );
							IsValidDecimal( (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE );
							IsValidDecimal( (int)TRIP_LINE.FIELDS.DISCOUNT_AMOUNT );

							InvoiveTotal += (double)Cells[ (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE ];
							break;

						case HEADER_COL.CHARGE_LINE:
							IsValidFieldCount( MaxEnum( typeof( CHARGE_LINE.FIELDS ) ) );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.ACCOUNT_ID );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.TRIP_ID );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.DESCRIPTION );
							IsValidDecimal( (int)CHARGE_LINE.FIELDS.VALUE );
							break;

						case HEADER_COL.END_OF_INVOICE:
							Cells[ (int)END_OF_INVOICE_LINE.FIELDS.TRIP_ID ] = "~~~~~~~~~~~~~~~~~";
							Cells[ (int)END_OF_INVOICE_LINE.FIELDS.INVOICE_TOTAL ] = InvoiveTotal;
							InvoiveTotal = 0;
							break;

						default:
							Errors.Add( "Unknown line type.", CurrentRecordIndex + 1 );
							break;
						}
					}
				}

				Func<Row, Row, int> SortCompare = ( a, b ) =>
				{
					int INumA;
					if( !int.TryParse( a[ (int)ACCOUNT_LINE.FIELDS.INVOICE_NUMBER ], out INumA ) )
						INumA = 0;

					int INumB;
					if( !int.TryParse( b[ (int)ACCOUNT_LINE.FIELDS.INVOICE_NUMBER ], out INumB ) )
						INumB = 0;

					var RetVal = INumA - INumB;

					if( RetVal == 0 )
					{
						RetVal = string.CompareOrdinal( a[ (int)ACCOUNT_LINE.FIELDS.ACCOUNT_ID ], b[ (int)ACCOUNT_LINE.FIELDS.ACCOUNT_ID ] );
						if( RetVal == 0 )
						{
							RetVal = string.CompareOrdinal( a[ (int)ACCOUNT_LINE.FIELDS.TRIP_ID ], b[ (int)ACCOUNT_LINE.FIELDS.TRIP_ID ] );
							if( RetVal == 0 )
							{
								var a0 = a[ 0 ];

								RetVal = string.CompareOrdinal( a0, b[ 0 ] );		// Type
								if( RetVal == 0 )
								{
									switch( a0 )
									{
									case HEADER_COL.CHARGE_LINE:
										RetVal = string.CompareOrdinal( a[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ], b[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] );	// Group Same Taxes and Charges together
										break;
									case HEADER_COL.TRIP:
										RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.SERVICE_LEVEL ], b[ (int)TRIP_LINE.FIELDS.SERVICE_LEVEL ] );
										if( RetVal == 0 )
											RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.PACKAGE_TYPE ], b[ (int)TRIP_LINE.FIELDS.PACKAGE_TYPE ] );
										break;
									}
								}
							}
						}
					}
					return RetVal;
				};

				if( !HasErrors )
				{
					try
					{
						Reader.Sort( ( a, b ) => { return SortCompare( a, b ); } );

						DoDebug( "After Internal Sort" );

						string LastColId = "",
								LastAccountId = "",
								LastTripId = "";


						Row SaveCells = null;

						// Summarise Trips & Charges
						for( CurrentRecordIndex = Reader.Rows - 1; CurrentRecordIndex >= 0; CurrentRecordIndex-- )
						{
							Cells = Reader[ CurrentRecordIndex ];
							var ColId = Cells[ 0 ];
							var AccountId = Cells[ (int)ACCOUNT_LINE.FIELDS.ACCOUNT_ID ];
							var TripId = Cells[ (int)ACCOUNT_LINE.FIELDS.TRIP_ID ];

							if( LastColId == ColId && LastAccountId == AccountId && LastTripId == TripId )
							{
								switch( ColId )
								{
								case HEADER_COL.CHARGE_LINE:
								case HEADER_COL.TRIP:
									if( SaveCells == null )
										SaveCells = Reader[ CurrentRecordIndex + 1 ];

									if( ColId == HEADER_COL.CHARGE_LINE )
									{
										if( SaveCells[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] == Cells[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] )	// Same Charge
										{
											var Val = double.Parse( SaveCells[ (int)CHARGE_LINE.FIELDS.VALUE ] ) + double.Parse( Cells[ (int)CHARGE_LINE.FIELDS.VALUE ] );
											SaveCells[ (int)CHARGE_LINE.FIELDS.VALUE ] = Val.ToString( "0.##" );
											Reader.RemoveAt( CurrentRecordIndex );
											continue;
										}
										SaveCells = null;
									}
									else
									{
										double Val;

										if( SaveCells[ (int)TRIP_LINE.FIELDS.SERVICE_LEVEL ] == Cells[ (int)TRIP_LINE.FIELDS.SERVICE_LEVEL ]
											&& SaveCells[ (int)TRIP_LINE.FIELDS.PACKAGE_TYPE ] == Cells[ (int)TRIP_LINE.FIELDS.PACKAGE_TYPE ] )	// Same Charge
										{
											var SaveColQty = SaveCells[ (int)TRIP_LINE.FIELDS.PIECES ].AsString.Trim();
											var ColQty = Cells[ (int)TRIP_LINE.FIELDS.PIECES ].AsString.Trim();

											if( SaveColQty != "" && ColQty != "" )
											{
												Val = double.Parse( SaveCells[ (int)TRIP_LINE.FIELDS.PIECES ] ) + double.Parse( Cells[ (int)TRIP_LINE.FIELDS.PIECES ] );
												SaveCells[ (int)TRIP_LINE.FIELDS.PIECES ] = Val.ToString( "0.##" );
											}
											else
												SaveCells[ (int)TRIP_LINE.FIELDS.PIECES ] = "";


											var SaveColWeight = SaveCells[ (int)TRIP_LINE.FIELDS.WEIGHT ].AsString.Trim();
											var ColWeight = Cells[ (int)TRIP_LINE.FIELDS.WEIGHT ].AsString.Trim();

											if( SaveColWeight != "" && ColWeight != "" )
											{
												Val = double.Parse( SaveCells[ (int)TRIP_LINE.FIELDS.WEIGHT ] ) + double.Parse( Cells[ (int)TRIP_LINE.FIELDS.WEIGHT ] );
												SaveCells[ (int)TRIP_LINE.FIELDS.WEIGHT ] = Val.ToString( "0.##" );
											}
											else
												SaveCells[ (int)TRIP_LINE.FIELDS.WEIGHT ] = "";

											Val = double.Parse( SaveCells[ (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE ] ) + double.Parse( Cells[ (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE ] );
											SaveCells[ (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE ] = Val.ToString( "0.##" );

											Reader.RemoveAt( CurrentRecordIndex );
											continue;
										}
										SaveCells = null;
									}
									break;
								default:
									SaveCells = null;
									break;
								}
							}
							else
								SaveCells = null;

							LastColId = ColId;
							LastAccountId = AccountId;
							LastTripId = TripId;
						}

						var CurrentInvoiceNumber = 0;
						double TripTotal = 0,
								ExpectedTripTotal = 0;

						string CurrentTripId = "",
								CurrentInvoice = "",
								CurrentAccountId = "";
						// Fix Up Missing Charges
						for( CurrentRecordIndex = 0; CurrentRecordIndex < Reader.Rows; ++CurrentRecordIndex )
						{
							try
							{
								Cells = Reader[ CurrentRecordIndex ];

								switch( Cells[ 0 ].AsString.Trim() )
								{
								case HEADER_COL.END_OF_INVOICE:
								case HEADER_COL.ACCOUNT:
									var Diff = ExpectedTripTotal - TripTotal;
									if( Math.Abs( Diff ) >= 0.01 )
									{
										var Ndx = Reader.AppendRow();
										var ChargeCells = Reader[ Ndx ];

										ChargeCells[ 0 ] = HEADER_COL.TEMPORY_CHARGE_HOLDER;
										ChargeCells[ (int)CHARGE_LINE.FIELDS.ACCOUNT_ID ] = CurrentAccountId;
										ChargeCells[ (int)CHARGE_LINE.FIELDS.INVOICE_NUMBER ] = CurrentInvoice;
										ChargeCells[ (int)CHARGE_LINE.FIELDS.TRIP_ID ] = CurrentTripId;
										ChargeCells[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] = "UNASSIGNED";				// "Total Amount Of Missing Charges";
										ChargeCells[ (int)CHARGE_LINE.FIELDS.VALUE ] = Diff;
									}
									TripTotal = ExpectedTripTotal = 0;

									CurrentAccountId = Cells[ (int)ACCOUNT_LINE.FIELDS.ACCOUNT_ID ];
									CurrentInvoice = Cells[ (int)ACCOUNT_LINE.FIELDS.INVOICE_NUMBER ];
									CurrentTripId = Cells[ (int)ACCOUNT_LINE.FIELDS.TRIP_ID ];
									break;

								case HEADER_COL.TRIP:
									ExpectedTripTotal = (double)Cells[ (int)TRIP_LINE.FIELDS.TOTAL_LIVE_VALUE ] + (double)Cells[ (int)TRIP_LINE.FIELDS.DISCOUNT_AMOUNT ];
									TripTotal = Cells[ (int)TRIP_LINE.FIELDS.BASE_RATE ].AsDouble;
									break;

								case HEADER_COL.CHARGE_LINE:
									TripTotal += Cells[ (int)CHARGE_LINE.FIELDS.VALUE ];
									break;
								}
							}
							catch( Exception E )
							{
								Errors.Add( "Error On Invoice " + CurrentInvoiceNumber.ToString() + ": (" + E.Message + ")", CurrentRecordIndex + 1 );
								CurrentInvoiceNumber = 0;
							}
						}

						for( CurrentRecordIndex = 0; CurrentRecordIndex < Reader.Rows; ++CurrentRecordIndex )
						{
							Cells = Reader[ CurrentRecordIndex ];

							if( Cells[ 0 ].AsString.Trim() == HEADER_COL.TEMPORY_CHARGE_HOLDER )
								Cells[ 0 ] = HEADER_COL.CHARGE_LINE;
						}

						Reader.Sort( ( a, b ) => { return SortCompare( a, b ); } );

						DoDebug( "After Summarise (Before Export)" );

						CurrentInvoiceNumber = 0;

						for( CurrentRecordIndex = 0; CurrentRecordIndex < Reader.Rows; ++CurrentRecordIndex )
						{
							try
							{
								Cells = Reader[ CurrentRecordIndex ];

								switch( Cells[ 0 ].AsString.Trim() )
								{
								case HEADER_COL.ACCOUNT:
									if( !int.TryParse( Cells[ (int)ACCOUNT_LINE.FIELDS.INVOICE_NUMBER ], out CurrentInvoiceNumber ) )
										CurrentInvoiceNumber = 0;

									highestInvoiceNumber = Math.Max( CurrentInvoiceNumber, highestInvoiceNumber );

									break;
								}
							}
							catch( Exception E )
							{
								Errors.Add( "Error On Invoice " + CurrentInvoiceNumber.ToString() + ": (" + E.Message + ")", CurrentRecordIndex + 1 );
								CurrentInvoiceNumber = 0;
							}
						}
					}
					catch( Exception E )
					{
						Errors.Add( "Exception Error: (" + E.Message + ")", CurrentRecordIndex + 1 );
					}
				}
				else
					return false;
			}
			catch( Exception E )
			{
				Errors.Add( "Exception Error: (" + E.Message + ")", CurrentRecordIndex + 1 );
			}
			finally
			{
				var Id = DateTime.Now.ToString( "yyyyMMddHHmmss" );
				string Path;

				if( HasErrors )
				{
					Path = errorPath;

					using( var Stream = new FileStream( errorPath + Id +  Globals.ERROR_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
					{
						errorText = Errors.ToString();
						Errors.WriteToStream( Stream );
					}
				}
				else
					Path = csvExportPath;

				using( var Stream = new FileStream( Path + Id +  Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
				{
					Csv.Write( Reader, Stream );
				}
			}
			return !HasErrors;
		}

		public bool ExportCsv( string fileName, bool debugMode, string csvExportPath, string errorPath, out int highestInvoiceNumber, out string errorText )
		{
			return ExportCsv( new FileStream( fileName, FileMode.Open, FileAccess.Read ), debugMode, csvExportPath, errorPath, out highestInvoiceNumber, out errorText );
		}
	}
}
