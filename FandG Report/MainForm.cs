﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Media;
using System.Windows.Forms;
using FandGReport.IDS;
using FandGReport.Properties;

namespace FandGReport
{
	public partial class MainForm : Form
	{
		private const string VERSION = " Vsn 1.03";

		private bool AllowClose = true;
		private bool IsDebugMode;
		private string ExportPath;

		public MainForm()
		{
			InitializeComponent();
		}


		private string Status
		{
			set
			{
				StatusLabel.Text = value;
				Application.DoEvents();
			}
		}

		private string Error
		{
			set
			{
				StatusLabel.ForeColor = Color.Crimson;
				StatusLabel.Text = value;
				Application.DoEvents();
			}
		}

		private void LoadSettings()
		{
			var D = Settings.Default;
			D.Reload();

			CsvExportPath.Text = D.CsvExportPath;
			DebugMode.Checked = D.DebugMode;
		}


		private void SaveSettings()
		{
			var D = Settings.Default;

			D.CsvExportPath = CsvExportPath.Text;
			D.DebugMode = DebugMode.Checked;

			D.Save();
		}

		private void MainForm_Load( object sender, EventArgs e )
		{
			StatusLabel.Text = "";
			LoadSettings();

			closeSettingsToolStripMenuItem_Click( sender, e );
		}

		private void exitToolStripMenuItem_Click( object sender, EventArgs e )
		{
			AllowClose = true;
			Close();
		}

		private void closeSettingsToolStripMenuItem_Click( object sender, EventArgs e )
		{
			CloseSettings.Enabled = false;
			var Pages = MainTabControl.TabPages;
			Pages.Remove( ImportSettingsTabPage );
			Pages.Remove( MainTabPage );
			Pages.Add( MainTabPage );

			SaveSettings();
		}

		private void ImportSettingsMenuItem_Click( object sender, EventArgs e )
		{
			CloseSettings.Enabled = true;
			var Pages = MainTabControl.TabPages;
			Pages.Remove( MainTabPage );
			Pages.Add( ImportSettingsTabPage );
		}


		// Needed because of Bug in radio buttons
		private void MainForm_Shown( object sender, EventArgs e )
		{
			// ReSharper disable once ConvertToConstant.Local
			var Version = VERSION;
#if DEBUG
			Version += " (TEST VERSION)";
#endif
			Text += Version;
		}

		private void MainForm_FormClosed( object sender, FormClosedEventArgs e )
		{
			SaveSettings();
		}

		private void button5_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = CsvExportPath.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				CsvExportPath.Text = FolderBrowserDialog.SelectedPath;
		}

		private void DoDebug( Csv csv, string fileName )
		{
			if( IsDebugMode )
			{
				try
				{
					Csv.WriteToFile( csv, ExportPath + '\\' +  fileName + Globals.CSV_EXTENSION );
				}
				catch
				{
				}
			}
		}


		private static Csv RemoveDuplicates( Csv trips )
		{
			var Result = new Csv();

			var TripRowCount = trips.Rows;

			var LastAccount = "";
			var LastTrip = "";

			for( var I = 0; I < TripRowCount; I++ )
			{
				var TripRow = trips[ I ];

				var AccountId = TripRow[ 0 ].AsString.Trim();
				TripRow[ 0 ].AsString = AccountId;

				var TripId = TripRow[ 23 ].AsString.Trim();
				TripRow[ 23 ].AsString = TripId;

				if( AccountId != LastAccount || TripId != LastTrip || I == 0 ) // Check for header
				{
					if( I != 0 )	// Not Header
					{
						LastTrip = TripId;
						LastAccount = AccountId;
					}

					Result.AppendRow();

					var ResultRow = Result[ Result.Rows - 1 ];
					var TripCellCount = TripRow.CellCount;

					for( var J = 0; J < TripCellCount; J++ )
						ResultRow[ J ] = TripRow[ J ];
				}
			}
			return Result;
		}


		private static Csv MergeCsv( Csv trips, Csv sales )
		{
			var Result = new Csv();

			var SalesRowCount = sales.Rows;
			var TripRowCount = trips.Rows;

			if( SalesRowCount > 0 && TripRowCount > 0 )
			{
				// Merge Header Lines
				Result.AppendRow();
				var ResultRow = Result[ Result.Rows - 1 ];

				var TripRow = trips[ 0 ];
				var SalesRow = sales[ 0 ];

				var TripCellCount = TripRow.CellCount;

				var Ndx = 0;
				for( var J = 0; J < TripCellCount; J++ )
					ResultRow[ Ndx++ ] = TripRow[ J ];

				var SalesCellCount = SalesRow.CellCount;

				for( var J = 0; J < SalesCellCount; J++ )
					ResultRow[ Ndx++ ] = SalesRow[ J ];


				for( var I = 1; I < SalesRowCount; I++ )
				{
					SalesRow = sales[ I ];

					var Account = SalesRow[ 0 ].AsString.Trim();
					var P = Account.IndexOf( " -\n", StringComparison.Ordinal );
					if( P >= 0 )
					{
						Account = Account.Substring( 0, P ).Trim();

						// Find Trip Recod
						for( var J = 1; J < TripRowCount; J++ )
						{
							TripRow = trips[ J ];

							var TripAccount = TripRow[ 0 ].AsString;
							if( TripAccount == Account )
							{
								Result.AppendRow();
								ResultRow = Result[ Result.Rows - 1 ];

								Ndx = 0;
								for( var K = 0; K < TripCellCount; K++ )
									ResultRow[ Ndx++ ] = TripRow[ K ];

								for( var K = 0; K < SalesCellCount; K++ )
									ResultRow[ Ndx++ ] = SalesRow[ K ];
							}
						}
					}
				}
			}
			return Result;
		}

		private void GetCsvBtn_Click( object sender, EventArgs e )
		{
			GetCsvBtn.Enabled = false;
			AllowClose = false;
			try
			{
				IsDebugMode = DebugMode.Checked;
				ExportPath = CsvExportPath.Text.TrimEnd( '\\' );
				
				StatusLabel.ForeColor = Color.DarkBlue;

				var Dte = Date.Value;

				Status = "Getting Sales";
				var SalesCsv = Communications.Sales( Dte, Dte );

				if( SalesCsv == null )
					Error = "Cannot Get Sales";
				else
				{
					DoDebug( SalesCsv, "Raw Sales Analysis" );

					Status = "Getting Trips";
					var TripsCsv = Communications.Trips( Dte, Dte );
					if( TripsCsv == null )
						Error = "Cannot Get Trips";
					else
					{
						DoDebug( TripsCsv, "Raw Trips" );

						Status = "Removing Duplicates";
						TripsCsv = RemoveDuplicates( TripsCsv );

						Status = "Merging Data";
						TripsCsv = MergeCsv( TripsCsv, SalesCsv );

						var FileName = ExportPath + "\\Merge - " + DateTime.Now.ToString( "yyyy-MM-dd hh-mm-ss" ) + ".csv";
						TripsCsv.WriteToFile( FileName );

						StatusLabel.ForeColor = Color.DarkGreen;
						Status = "Completed";

						Process.Start( CsvExportPath.Text );
					}
				}
			}
			finally
			{
				GetCsvBtn.Enabled = true;
				AllowClose = true;
			}
		}

		private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( !AllowClose )
			{
				e.Cancel = true;
				SystemSounds.Beep.Play();
			}
		}
	}
}