﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FandGReport.IDS.Http;

namespace FandGReport.IDS
{
	internal class Communications
	{
		private const string
			SalesLink =
				"http://reports.internetdispatcher.org/ids/reports?carrierId=fandg&accountId=fandg&userId=ray&report=SalesAnalysis&output=4&targetDriverId=+&targetStaffId=+&targetAccountId=&from=~FDATE~&to=~TDATE~&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true",
//			TripLink =
//				"http://reports.internetdispatcher.org/ids/reports?carrierId=fandg&accountId=fandg&userId=ray&report=GeneralTripReport&output=4&targetDriverId=+&targetStaffId=+&targetAccountId=&from=~FDATE~&to=~TDATE~&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true",


//			TripLink =
//				"http://reports.internetdispatcher.org/ids/reports?carrierId=fandg&accountId=fandg&userId=ray&report=CustomCSVExportPlaceHolderFandG&output=99&targetDriverId=+&targetStaffId=+&targetAccountId=&from=~FDATE~&to=~TDATE~&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";


			TripLink = "http://reports.internetdispatcher.org/ids/reports?carrierId=fandg&accountId=fandg&userId=ray&report=GeneralTripReportFandG&output=4&targetDriverId=+&targetStaffId=+&targetAccountId=&from=~FDATE~&to=~TDATE~&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";



		private readonly string Link;

		private Communications( string link )
		{
			Link = link;
		}


		private static string SetDate( DateTime fromDate, DateTime toDate, string link )
		{
			const string Fmt = "yyyy/MM/dd";

			var FDate = fromDate.ToString( Fmt );
			var TDate = toDate.ToString( Fmt );

			return link.Replace( "~FDATE~", FDate ).Replace( "~TDATE~", TDate );
		}

		public static Csv Sales( DateTime fromDate, DateTime toDate )
		{
			return new Communications( SetDate( fromDate, toDate, SalesLink ) ).GetCsv();
		}

		public static Csv Trips( DateTime fromDate, DateTime toDate )
		{
			return new Communications( SetDate( fromDate, toDate, TripLink ) ).GetCsv();
		}

		public static Csv Trips( DateTime date )
		{
			return Trips( date, date );
		}

		private Csv GetCsv( Action<string> onError = null )
		{
			var Completed = false;
			var ReplyError = false;

			var Csv = "";

			Task.Run( () =>
			{
				try
				{
					var Reason = "";

					HttpIo.WebIo.Get( new HttpIo.RequestEntry( Link, Reply => { Csv = Reply.Content; }, Reply =>
					{
						ReplyError = true;
						Reason = Reply.Content.Trim();
					}, true ) );

					if( onError != null && ( ReplyError || Csv.Contains( Reason = "Authentication Error" ) ) )
					{
						onError( Reason );
					}
				}
				finally
				{
					Completed = true;
				}
			} );

			while( !Completed )
				Application.DoEvents();

			if( ReplyError )
				return null;

			var Stream = new MemoryStream( Encoding.UTF8.GetBytes( Csv ) ) { Position = 0 };

			return new Csv( Stream );
		}
	}
}