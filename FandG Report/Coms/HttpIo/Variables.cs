
using System.Net;
using System.Net.Cache;
using System.Threading.Tasks;

namespace FandGReport.IDS.Http
{
	public partial class HttpIo
	{
		public const int DEFAULT_TIMEOUT = 10 * 60 * 1000;	

		static HttpRequestCachePolicy NoCachePolicy = new HttpRequestCachePolicy( HttpRequestCacheLevel.NoCacheNoStore );

		HttpIo.RequestQueue WebIoQueue = new HttpIo.RequestQueue();
		Task PostTask;
		bool Closing;

		public static object CookieLock = new object();

		public static CookieContainer GlobalCookies = new CookieContainer();
	}
}
