﻿using System.Net;

namespace FandGReport.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		private static void InitCookie()
		{
			lock( CookieLock )
			{
				GlobalCookies = new CookieContainer();
			}
		}
	}
}