﻿using System;
using System.IO;
using System.Text;
using FandGReport.Export;

namespace FandGReport.IDS
{
	internal class IdsReMap
	{
		private static string[] Colons =  new string[] { "::" };

		internal enum FIELDS
		{
			IDS_INVOICE_NUMBER,			// A
			IDS_INVOICE_DATE_CREATED,	// B
			IDS_READY_TIME,				// C
			IDS_INVOICE_TO_DATE,		// D
			IDS_SHIPMENT_ID,			// E
			IDS_ACCOUNT_ID,				// F
			IDS_CALL_TIME,				// G

			IDS_PICKUP_COMPANY_NAME,	// H 
			IDS_PICKUP_SUITE,			// I
			IDS_PICKUP_STREET,			// J
			IDS_PICKUP_CITY,			// K
			IDS_PICKUP_REGION,			// L
			IDS_PICKUP_COUNTRY,			// M
			IDS_PICKUP_POSTAL_CODE,		// N
			IDS_PICKUP_ZONE,			// O

			IDS_DELIVERY_COMPANY_NAME,	// P
			IDS_DELIVERY_SUITE,			// Q
			IDS_DELIVERY_STREET,		// R
			IDS_DELIVERY_CITY,			// S
			IDS_DELIVERY_REGION,		// T
			IDS_DELIVERY_COUNTRY,		// U
			IDS_DELIVERY_POSTAL_CODE,	// V
			IDS_DELIVERY_ZONE,			// W

			IDS_BILLING_COMPANY_NAME,	// X
			IDS_BILLING_SUITE,			// Y
			IDS_BILLING_STREET,			// Z
			IDS_BILLING_CITY,			// AA
			IDS_BILLING_REGION,			// AB
			IDS_BILLING_COUNTRY,		// AC
			IDS_BILLING_POSTAL_CODE,	// AD
			IDS_BILLING_PHONE,			// AE

			IDS_WEIGHT,					// AF
			IDS_PIECES,					// AG

			IDS_SERVICE_LEVEL,			// AH
			IDS_PACKAGE_TYPE,			// AI
			IDS_BASE_RATE,				// AJ
			IDS_TOTAL_TAX,				// AK
			IDS_INVOICE_TOTAL_INCLUDING_TAX,	// AL

			IDS_DELIVERY_DRIVER_NAME,	// AM
			IDS_CALL_TAKER_ID,			// AN

			IDS_SORT_ORDER,				// AO
			IDS_DRIVER_REFERENCE,		// AP		
			IDS_DISCOUNT_AMOUNT,		// AQ

			IDS_SHIPPING_SUITE,			// AR
			IDS_SHIPPING_STREET,		// AS
			IDS_SHIPPING_CITY,			// AT
			IDS_SHIPPING_REGION,		// AU
			IDS_SHIPPING_COUNTRY,		// AV
			IDS_SHIPPING_POSTAL_CODE,	// AW

			IDS_PICKUP_TIME,			// AX
			IDS_DELEVERY_TIME,			// AY

			IDS_BEGIN_CHARGES			// AZ
		}

		// 20060921_230520  --> 2006-06-021 23:05:20
		private static string FixDate( string FunnyDate )
		{
			var Parts = FunnyDate.Trim().Split( '_' );
			if( Parts.Length == 2 )
			{
				string Date = Parts[ 0 ].Substring( 0, 4 ) + "-" + Parts[ 0 ].Substring( 4, 2 ) + "-" + Parts[ 0 ].Substring( 6, 2 ),
						Time = Parts[ 1 ].Substring( 0, 2 ) + ":" + Parts[ 1 ].Substring( 2, 2 ) + ":" + Parts[ 1 ].Substring( 4, 2 );

				return Date + " " + Time;
			}
			return "00-00-00 00:00:00";
		}

		internal static Stream ReMapCsv( Stream csv, bool debugMode, string errorPath, int maxInvoiceNumber )
		{
			errorPath = Utils.AddPathSeparator( errorPath );

			csv.Position = 0;

			var Reader = new Csv( csv );

			Action<string> DoDebug = ( fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						Csv.WriteToFile( Reader, errorPath + fileName + Globals.CSV_EXTENSION );
					}
					catch
					{
					}
				}
			};

			Action<StringBuilder, string> DoDebugText = ( csvText, fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						using( var Stream = new FileStream( errorPath + fileName + Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
						{
							var Bytes = System.Text.Encoding.UTF8.GetBytes( csvText.ToString() );
							Stream.Write( Bytes, 0, Bytes.Length );
						}
					}
					catch
					{
					}
				}
			};

			for( var I = Reader.Rows; --I >= 0; )
			{
				var Inv = Reader[ I ][ (int)FIELDS.IDS_INVOICE_NUMBER ].AsString;

				int InvNumber;

				if( !int.TryParse( Inv, out InvNumber ) )
					InvNumber = 0;

				if( InvNumber <= maxInvoiceNumber )
					break;

				Reader.RemoveAt( I );
			}
			
			DoDebug( "After Ids Import (Before Sort)" );

			// Sort By Invoice Number / Call Time
			Reader.Sort( ( a, b ) =>
			{
				var RetVal = string.CompareOrdinal( a[ (int)FIELDS.IDS_ACCOUNT_ID ], b[ (int)FIELDS.IDS_ACCOUNT_ID ] );
				if( RetVal == 0 )
				{
					int INumA;
					if( !int.TryParse( a[ (int)FIELDS.IDS_INVOICE_NUMBER ], out INumA ) )
						INumA = 0;

					int INumB;
					if( !int.TryParse( b[ (int)FIELDS.IDS_INVOICE_NUMBER ], out INumB ) )
						INumB = 0;

					RetVal = INumA - INumB;

					if( RetVal == 0 )
						RetVal = string.CompareOrdinal( a[ (int)FIELDS.IDS_SHIPMENT_ID ], b[ (int)FIELDS.IDS_SHIPMENT_ID ] );
				}
				return RetVal;
			} );

			DoDebug( "After Ids Import (After Sort)" );

			StringBuilder CsvFile = new StringBuilder(),
						  Line = new StringBuilder();

			string	LastInv = "",
					LastAccountId = "",
					LastTripId = "";

			Action<string> AddField = ( string Field ) =>
			{
				if( Line.Length > 0 )
					Line.Append( "," );

				Line.Append( '"' + Field + '"' );
			};

			Action EndOfLine = () =>
			{
				CsvFile.Append( Line + "\r\n" );
				Line.Clear();
			};

			Action EndOfInvoice = () =>
			{
				if( LastAccountId != "" )
				{
					// Build Account Line
					AddField( IdsInvoiceExport.HEADER_COL.END_OF_INVOICE );	// Line Type Id
					AddField( LastAccountId );
					AddField( LastInv );
					EndOfLine();
				}
			};

			foreach( Row ImportLine in Reader )
			{
				var AccountId = ImportLine[ (int)FIELDS.IDS_ACCOUNT_ID ].AsString;

						//Remove trailing period BUG at IDS
				var L = AccountId.Length;
				if( L-- <= 0 )
					continue;

				if( AccountId[ L ] == '.' )
				{
					if( L == 0 )
						continue;

					AccountId = AccountId.Substring( 0, L );
				}

				var Inv = ImportLine[ (int)FIELDS.IDS_INVOICE_NUMBER ];
				var TripId = ImportLine[ (int)FIELDS.IDS_SHIPMENT_ID ];
				var Eoi = ( LastAccountId != AccountId || LastInv != Inv );

				if( Eoi || TripId != LastTripId )
				{
					if( Eoi )
						EndOfInvoice();

					LastAccountId = AccountId;
					LastInv = Inv;
					LastTripId = TripId;


					// Build Account Line
					AddField( IdsInvoiceExport.HEADER_COL.ACCOUNT );	// Line Type Id

					AddField( AccountId );												// ACCOUNT_ID
					AddField( Inv );													// INVOICE_NUMBER
					AddField( TripId );													// TRIP_ID

					AddField( FixDate( ImportLine[ (int)FIELDS.IDS_INVOICE_DATE_CREATED ] ) );	// DRIVER_ASSIGN_TIME,
					AddField( FixDate( ImportLine[ (int)FIELDS.IDS_READY_TIME ] ) );			// READY_TIME,
					AddField( FixDate( ImportLine[ (int)FIELDS.IDS_INVOICE_TO_DATE ] ) );		// INVOICE_DATE_TIME,
					AddField( ImportLine[ (int)FIELDS.IDS_CALL_TIME ] );						// CALL_TIME,

					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_TIME ] );				// PICKUP TIME
					AddField( ImportLine[ (int)FIELDS.IDS_DELEVERY_TIME ] );			// DELIVERY TIME

					AddField( "" );														// PICKUP_REFERENCE
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_COMPANY_NAME ] );		// PICKUP_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_SUITE ] );				// PICKUP_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_STREET ] );			// PICKUP_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_CITY ] );				// PICKUP_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_REGION ] );			// PICKUP_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_COUNTRY ] );			// PICKUP_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_POSTAL_CODE ] );		// PICKUP_POST_CODE
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_ZONE ] );				// PICKUP_ZONE
					AddField( "" );														// PICKUP_CONTACT
					AddField( "" );														// PICKUP_TELEPHONE
					AddField( "" );														// PICKUP_EMAIL
					AddField( "" );														// PICKUP_NOTES

					AddField( "" );														// DELIVERY_REFERENCE
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_COMPANY_NAME ] );	// DELIVERY_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_SUITE ] );			// DELIVERY_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_STREET ] );			// DELIVERY_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_CITY ] );			// DELIVERY_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_REGION ] );			// DELIVERY_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_COUNTRY ] );			// DELIVERY_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_POSTAL_CODE ] );		// DELIVERY_POST_CODE
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_ZONE ] );			// DELIVERY_ZONE
					AddField( "" );														// DELIVERY_CONTACT
					AddField( "" );														// DELIVERY_TELEPHONE
					AddField( "" );														// DELIVERY_EMAIL
					AddField( "" );														// DELIVERY_NOTES

					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COMPANY_NAME ] );		// BILLING_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_SUITE ] );			// BILLING_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_STREET ] );			// BILLING_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_CITY ] );				// BILLING_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_REGION ] );			// BILLING_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COUNTRY ] );			// BILLING_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_POSTAL_CODE ] );		// BILLING_POST_CODE
					AddField( "" );														// BILLING_CONTACT
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_PHONE ] );			// BILLING_TELEPHONE
					AddField( "" );														// BILLING_EMAIL

					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COMPANY_NAME ] );		// SHIPPING_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_SUITE ] );			// SHIPPING_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_STREET ] );			// SHIPPING_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_CITY ] );			// SHIPPING_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_REGION ] );			// SHIPPING_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_COUNTRY ] );			// SHIPPING_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_POSTAL_CODE ] );		// SHIPPING_POST_CODE
					AddField( "" );														// SHIPPING_CONTACT
					AddField( "" );														// SHIPPING_TELEPHONE
					AddField( "" );														// SHIPPING_EMAIL

					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_DRIVER_NAME ] );		// DRIVER_NAME
					AddField( ImportLine[ (int)FIELDS.IDS_CALL_TAKER_ID ] );			// TRAILER_DRIVER_NAME

					AddField( ImportLine[ (int)FIELDS.IDS_SORT_ORDER ] );				// SORT_ORDER
					AddField( ImportLine[ (int)FIELDS.IDS_DRIVER_REFERENCE ] );			// DRIVER_REFERENCE

					EndOfLine();
				}

					// Build Trip Line
				AddField( IdsInvoiceExport.HEADER_COL.TRIP );							// Line Type Id
				AddField( AccountId );													// CUSTOMER_NAME
				AddField( Inv );														// INVOICE_NUMBER
				AddField( TripId );														// TRIP_ID

				AddField( ImportLine[ (int)FIELDS.IDS_SERVICE_LEVEL ] );				// SERVICE_LEVEL
				AddField( ImportLine[ (int)FIELDS.IDS_PACKAGE_TYPE ] );					// PACKAGE_TYPE
				AddField( ImportLine[ (int)FIELDS.IDS_PIECES ] );						// PIECES
				AddField( ImportLine[ (int)FIELDS.IDS_WEIGHT ] );						// WEIGHT
				AddField( ImportLine[ (int)FIELDS.IDS_BASE_RATE ] );					// PRICE
				AddField( ImportLine[ (int)FIELDS.IDS_INVOICE_TOTAL_INCLUDING_TAX ] );	// TOTAL_LIVE_VALUE
				AddField( ImportLine[ (int)FIELDS.IDS_DISCOUNT_AMOUNT ] );				// DISCOUNT_AMOUNT

				EndOfLine();

				for( int I = (int)FIELDS.IDS_BEGIN_CHARGES, C = ImportLine.Count; I < C; )
				{
					var Charge = ImportLine[ I++ ].AsString.Trim();
					if( Charge != "" )
					{
						var Parts = Charge.Split( Colons, StringSplitOptions.RemoveEmptyEntries );
						if( Parts.Length == 2 )
						{
							// Build Charge Lines
							AddField( IdsInvoiceExport.HEADER_COL.CHARGE_LINE );			// Line Type Id

							if( Parts[ 0 ].StartsWith( "CHG_" ) )
								Parts[ 0 ] = Parts[ 0 ].Substring( 4 );

							AddField( AccountId );								// CUSTOMER_NAME
							AddField( Inv );									// INVOICE_NUMBER
							AddField( LastTripId );								// TRIP_ID

							if( Parts[ 0 ] == "GST" )
								Parts[ 1 ] = ImportLine[ (int)FIELDS.IDS_TOTAL_TAX ];

							AddField( Parts[ 0 ] );								// DESCRIPTION
							AddField( Parts[ 1 ] );								// VALUE

							EndOfLine();
						}
					}
					else
						break;
				}
			}

			EndOfInvoice();

			DoDebugText( CsvFile, "After Re-Map" );

			return new MemoryStream( System.Text.Encoding.UTF8.GetBytes( CsvFile.ToString() ) );
		}

		internal static Stream ReMapCsv( string Csv, bool debugMode, string errorPath, int maxInvoiceNumber )
		{
			using( var Stream = new MemoryStream( System.Text.Encoding.UTF8.GetBytes( Csv ) ) )
				return ReMapCsv( Stream, debugMode, errorPath, maxInvoiceNumber );
		}
	}
}
