﻿using System;
using System.Collections;
using System.IO;
using System.Text;

namespace FandGReport
{
	public class Csv : IEnumerable
	{
		internal enum TOKENS : ushort
		{
			COMMA_IN_QUOTE = ushort.MaxValue,
			COMMA = COMMA_IN_QUOTE - 1,
			NEW_LINE_IN_QUOTE = COMMA - 1
		};


		private readonly Rows _Rows = new Rows();


		public IEnumerator GetEnumerator()
		{
			return _Rows.GetEnumerator();
		}


		public Row this[ int ndx ]
		{
			get { return _Rows[ ndx ]; }
		}


		public void RemoveAt( int ndx )
		{
			_Rows.RemoveAt( ndx );
		}

		public int AppendRow()
		{
			return _Rows.AppendRow();
		}

		public void Sort( Comparison<Row> func )
		{
			_Rows.Sort( func );
		}

		public int Rows
		{
			get { return _Rows.Count; }
		}

		public override string ToString()
		{
			return _Rows.AsFileString;
		}

		public Csv()
		{
		}

		public Csv( string csvData )
		{
			var StringData = new StringBuilder( csvData );

			// Clean Up Lines
			StringData.Replace( "\r\n", "\n" );
			StringData.Replace( "\r", "" );

			// Fix Empty Cells
			StringData.Replace( ",\"\",", ",," );
			StringData.Replace( "\"\"\n", "\n" );

			var Len = StringData.Length;

			var StringData2 = new StringBuilder();
			var InQuote = false;

			// Tokenise commas in quotes
			for( var I = 0; I < Len; I++ )
			{
				var C = StringData[ I ];

				// Beginning or end of a quote sequence (Loose the quotes)
				if( C == '"' )
				{
					if( InQuote )
					{
						// Look ahead for a Double Quote (Escaped Quote)
						var C1 = StringData[ I + 1 ];
						if( C1 == '"' )
						{
							StringData2.Append( C1 );
							++I;
						}
						else
							InQuote = false;
					}
					else
						InQuote = true;
				}
				else
				{
					if( InQuote )
					{
						switch( C )
						{
						case ',':
							C = (char) TOKENS.COMMA_IN_QUOTE;
							break;
						case '\n':
							C = (char) TOKENS.NEW_LINE_IN_QUOTE;
							break;
						}
					}
					StringData2.Append( C );
				}
			}


			var Lines = StringData2.ToString().TrimEnd().Split( '\n' );

				// Put Cell Specials back in Quotes
			for( var I = 0; I < Lines.Length; I++ )
			{
				Lines[ I ] = Lines[ I ].Replace( ',', (char) TOKENS.COMMA )		// Must Be First
					.Replace( (char) TOKENS.COMMA_IN_QUOTE, ',' )
					.Replace( (char) TOKENS.NEW_LINE_IN_QUOTE, '\n' );
			}

			_Rows = new Rows();

			Len = Lines.Length;

			for( var I = 0; I < Len; I++ )
				_Rows.AddRow( I, Lines[ I ] );
		}

		private static string StreamToString( Stream stream )
		{
			using( var Reader = new StreamReader( stream, Encoding.ASCII ) )
			{
				stream.Position = 0;
				return Reader.ReadToEnd();
			}
		}

		public Csv( Stream csvData ) : this( StreamToString( csvData ) )
		{
		}

		public static Csv Read( string csvData )
		{
			return new Csv( csvData );
		}

		public static Csv Read( Stream csvData )
		{
			return new Csv( csvData );
		}

		public static Csv ReadFromFile( string fileName )
		{
			using( var Stream = new FileStream( fileName, FileMode.Open, FileAccess.Read, FileShare.None ) )
			{
				return new Csv( Stream );
			}
		}

		public static void Write( Csv csvData, Stream stream )
		{
			using( var Writer = new StreamWriter( stream, Encoding.UTF8 ) )
			{
				stream.Position = 0;

				var Data = csvData.ToString();
				Writer.Write( Data );
			}
		}

		public void Write( Stream stream )
		{
			Write( this, stream );
		}

		public static void WriteToFile( Csv csvData, string fileName )
		{
			using( var Stream = new FileStream( fileName, FileMode.Create, FileAccess.Write, FileShare.None ) )
			{
				Write( csvData, Stream );
			}
		}

		public void WriteToFile( string fileName )
		{
			WriteToFile( this, fileName );
		}
	}
}