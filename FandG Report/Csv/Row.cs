﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FandGReport
{
	public class OrderedDictionary<TIndex,TData> : List<TData>
	{
		private Dictionary<TIndex, TData> Dict = new Dictionary<TIndex, TData>();

		public TData this[ TIndex index ]
		{
			get
			{
				return Dict[ index ];
			}

			protected set
			{
				if( ContainsKey( index ) )
				{
					var Old = Dict[ index ];
					var Ndx = IndexOf( Old );
					base[ Ndx ] = value;
					Dict[ index ] = value;
				}
				else
					Add( index, value );
			}
		}

		protected void Add( TIndex index, TData data )
		{
			Add( data );
			Dict.Add( index, data );
		}

		protected bool ContainsKey( TIndex index )
		{
			return Dict.ContainsKey( index );
		}

		public void RemoveAt( TIndex ndx )
		{
			if( ContainsKey( ndx ) )
			{
				var Data = Dict[ ndx ];
				Dict.Remove( ndx );
				base.RemoveAt( IndexOf( Data ) );
			}
		}
	}


	public class Row : List<Cell>
	{
		internal int CellCount
		{
			get
			{
				return Count;
			}
		}

		new public Cell this[ int index ]
		{
			get
			{
				return base[ index ];
			}

			set
			{
				var I = index;
				var	C = Count;

				for( ; I <= C; I++ )
					Add( new Cell() );

				base[ index ] = value;
			}
		}

		internal string AsFileString( int totalColumns )
		{
			int I, C;
			var First = true;
			var Result = new StringBuilder();

			for( I = 0, C = Count; I < C; I++ )
			{
				if( !First )
					Result.Append( ',' );
				else
					First = false;

				Result.Append( base[ I ].AsFileString );
			}

			for( ; I++ < totalColumns; )
				Result.Append( ',' );

			return Result.ToString();
		}
	}

	public class Rows : OrderedDictionary<int, Row>
	{
		public int AppendRow()
		{
			var NewRow = Count;
			AddRow( NewRow, new Row() );
			return NewRow;
		}

		private void AddRow( int ndx, Row row )
		{
			if( ContainsKey( ndx ) )
				this[ ndx ] = row;
			else
				Add( ndx, row );
		}

		internal void AddRow( int ndx, string row )
		{
			var Cells = row.Split( (char)Csv.TOKENS.COMMA );

			var NewRow = new Row();

			foreach( var CellContent in Cells )
			{
				var NewCell = new Cell();
				NewCell.AsString = CellContent;
				NewRow.Add( NewCell );
			}

			AddRow( ndx, NewRow );
		}

		private int Columns
		{
			get
			{
				var MaxCols = 0;

				foreach( var Rows in this )
					MaxCols = Math.Max( MaxCols, Rows.CellCount );

				return MaxCols;
			}
		}

		internal string AsFileString
		{
			get
			{
				var Result = new StringBuilder();

				var MaxCols = Columns;

				// ReSharper disable once LoopCanBePartlyConvertedToQuery
				foreach( var Rows in this )
				{
					var Data = Rows.AsFileString( MaxCols ) + "\r\n";
					Result.Append( Data );
				}

				return Result.ToString();
			}
		}
	}
}
